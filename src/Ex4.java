import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {

        Scanner sc = null;
        File fichero;

        try {
            System.out.println("Escribe el fichero a leer");
            sc = new Scanner(System.in);
            fichero = new File(sc.nextLine());
            sc.close();

            FileReader file=new FileReader(fichero);
            int data = file.read();

            while (data!=-1){
                System.out.print((char)data);
                data=file.read();
            }



        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            sc.close();
        }
    }
}

