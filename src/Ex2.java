import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex2 {

    public static void main(String[] args) throws IOException {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introduce la ruta y el fichero que quieres crear");
        File nouficher = new File(lector.nextLine());

        System.out.println("Quantes lineos vilñs excriute");
        int lines = lector.nextInt();
        lector.nextLine();
        String linea;

        try {

            if (!nouficher.exists()) {
                nouficher.createNewFile();
            }
            else {
                System.out.println("El fitxer ja existeix");
            }
            int i=0;
            FileWriter escribir=new FileWriter(nouficher);

            while (lines>i){
                System.out.println("Linea "+(i+1));
                linea=lector.nextLine();
                escribir.write(linea+"\n");
                i++;
            }
            escribir.close();

        }
        catch (IOException e){
            System.out.println("Error de entrada y salida");
        }
    }
}
