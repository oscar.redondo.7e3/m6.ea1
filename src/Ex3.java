import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex3 {

    public static void main(String[] args)  {

        Scanner lector = new Scanner(System.in);
        System.out.println("Introduce a que fichero quieres añadir algo");
        String ruta= lector.nextLine();
        System.out.println("Introduce la frase que quieres añadir");
        String frase=lector.nextLine();

        try {
            System.out.println(frase);
            System.out.println(ruta);
            FileWriter nouficher = new FileWriter(ruta,true);
            nouficher.write("\n"+frase);

            nouficher.close();
        }

        catch (IOException e){
            System.out.println("No existe");

        }

    }
}
