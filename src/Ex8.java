import java.io.File;
import java.util.Scanner;

public class Ex8 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introduce a que fichero eliminar");
        File ruta= new File(lector.nextLine());
        if (ruta.exists()) {
            ruta.delete();
        }
        else System.out.println("No exiteix el fitxers");
    }
}
