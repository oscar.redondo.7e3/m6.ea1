import java.io.File;
import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {

        Scanner sc = null;
        File fichero;

        try {
            System.out.println("Escribe el fichero a leer");
            sc = new Scanner(System.in);
            fichero = new File(sc.nextLine());
            sc.close();


            sc = new Scanner(fichero);

            while (sc.hasNext()) {
                System.out.println(sc.nextLine());
            }

        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            sc.close();
        }
    }
}
