import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introduce la ruta");
        String ruta = lector.nextLine();

        try {
            File path = new File(ruta);
            if (path.exists() && path.isDirectory()){
                    File[] contents= path.listFiles();
                    for (File content :contents){
                        if (content.isDirectory()){
                            System.out.println("[Dir]: "+content.getName());
                        }
                        else{
                            System.out.println("[File]: " + content.getName());
                        }

                    }
            }
            else {
                System.out.println("No existe o no es un directorio");
            }

        }
        catch (Exception e){
            System.out.println("Error");
        }

    }
}
